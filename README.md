### In the beginning, one time
* docker-compose up -d workspace  //(should be in dir docker-geohosting-server)
* docker exec --user laradock -it dockergeohostingserver_workspace_1 bash
* npm install

### Start production server
* docker-compose up -d mongo redis node

### Start development server
* docker-compose up -d mongo redis workspace
* docker exec --user laradock -it dockergeohostingserver_workspace_1 bash
* ./node_modules/.bin/gulp

### Workspace
* docker exec --user laradock -it dockergeohostingserver_workspace_1 bash

### In workspace, gulp (front-end generation)
* ./node_modules/.bin/gulp

### Useful commands ###

List all containers (only IDs)

* docker ps -aq.

Stop all running containers.

* docker stop $(docker ps -aq)

Remove all containers.

* docker rm $(docker ps -aq)

Remove all images.

* docker rmi $(docker images -q)